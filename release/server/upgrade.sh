#!/usr/bin/env bash
# Upgrades the system running in a server
#
# Assumes the directory to be ${_running_dir}, and that everything is configured
# through docker compose.

_upgrade_dir="/tmp/kikilista-upgrade"
_running_dir="/app/kikilista"
_image="gabaconrado/kikilista:latest"

set -e

cd ${_running_dir}
echo "Stopping service"
docker compose down
echo "Upgrading image"
docker pull ${_image}
docker image prune -f
echo "Upgrading files"
mv ${_upgrade_dir}/compose.yml .
mv ${_upgrade_dir}/nginx.conf .

docker compose up -d
cd -

set +e
