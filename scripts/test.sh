#!/usr/bin/env bash
# Tests the app with all features and targets enabled

cargo test --all-features --all-targets
