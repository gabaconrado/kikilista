#!/usr/bin/env bash
# Builds the docker image to run the Kikilista app

_image_name="kikilista"

docker build -t ${_image_name} -f release/docker/Dockerfile .
