#!/usr/bin/env bash
# Builds the app with all features and targets

cargo build --all-features --all-targets
