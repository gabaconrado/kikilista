#!/usr/bin/env bash
# Builds the app with all features and targets

cargo clippy --all-features --all-targets
