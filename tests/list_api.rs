//! This test asserts the behavior of the list API
//!
//! - Starts the Axum API in an available socket in the machine;
//! - Creates the Reqwest based client;
//! - Performs requests and assert the responses;
//! - All structures used are in memory;

use std::{collections::HashMap, future::IntoFuture, str::FromStr};

use kikilista::{
    AxumKikilistaServer, InMemoryListStorage, InMemoryTokenStorage, Item, ItemName, ItemState,
    List, ListName, ReqwestKikilistaClient, Token, Username,
};
use tokio::net::TcpListener;
use url::Url;

/// Common test utilities
mod fixtures;

#[tokio::test]
async fn should_create_list() {
    let (server, client, listener) = setup_test().await;
    let list_name = ListName::new(fixtures::random_string());

    tokio::spawn(axum::serve(listener, server.into_router()).into_future());

    let _ = client
        .create_list(list_name)
        .await
        .expect("Error calling create list");
}

#[tokio::test]
async fn should_all_lists() {
    let (server, client, listener) = setup_test().await;
    let list_name_1 = ListName::new(fixtures::random_string());
    let list_name_2 = ListName::new(fixtures::random_string());

    tokio::spawn(axum::serve(listener, server.into_router()).into_future());

    let list_id_1 = client
        .create_list(list_name_1.clone())
        .await
        .expect("Error calling create list");
    let list_id_2 = client
        .create_list(list_name_2.clone())
        .await
        .expect("Error calling create list");

    let mut all_lists = client.all_lists().await.expect("Error calling all lists");
    all_lists.sort_by_key(|l| l.id());
    let list_1 = &all_lists[0];
    let list_2 = &all_lists[1];
    let expected_list_1 = List::new(list_id_1, list_name_1);
    let expected_list_2 = List::new(list_id_2, list_name_2);

    assert_eq!(all_lists.len(), 2);
    assert_eq!(expected_list_1, *list_1);
    assert_eq!(expected_list_2, *list_2);
}

#[tokio::test]
async fn should_show_list() {
    let (server, client, listener) = setup_test().await;
    let list_name = ListName::new(fixtures::random_string());

    tokio::spawn(axum::serve(listener, server.into_router()).into_future());

    let list_id = client
        .create_list(list_name.clone())
        .await
        .expect("Error calling create list");

    let list = client
        .show_list(list_id)
        .await
        .expect("Error calling show list");

    let expected_list = List::new(list_id, list_name);

    assert_eq!(list, expected_list);
}

#[tokio::test]
async fn should_remove_list() {
    let (server, client, listener) = setup_test().await;
    let list_name = ListName::new(fixtures::random_string());

    tokio::spawn(axum::serve(listener, server.into_router()).into_future());

    let list_id = client
        .create_list(list_name.clone())
        .await
        .expect("Error calling create list");

    let _list_present = client
        .show_list(list_id)
        .await
        .expect("Error running show list");

    client
        .remove_list(list_id)
        .await
        .expect("Error calling show list");

    let all_lists = client.all_lists().await.expect("Error running remove list");

    assert!(all_lists.is_empty());
}

#[tokio::test]
async fn should_add_item() {
    let (server, client, listener) = setup_test().await;
    let list_name = ListName::new(fixtures::random_string());
    let item_name_1 = ItemName::new(fixtures::random_string());
    let item_name_2 = ItemName::new(fixtures::random_string());

    tokio::spawn(axum::serve(listener, server.into_router()).into_future());

    let list_id = client
        .create_list(list_name.clone())
        .await
        .expect("Error calling create list");

    let item_id_1 = client
        .add_item(list_id, item_name_1.clone())
        .await
        .expect("Error running add item");

    let item_id_2 = client
        .add_item(list_id, item_name_2.clone())
        .await
        .expect("Error running add item");

    let list = client
        .show_list(list_id)
        .await
        .expect("Error running show list");

    let mut items = list.items().cloned().collect::<Vec<Item>>();
    items.sort_by_key(|i| i.id());

    let expected_item_1 = Item::new(item_id_1, item_name_1);
    let expected_item_2 = Item::new(item_id_2, item_name_2);

    assert_eq!(items.len(), 2);
    assert_eq!(items[0], expected_item_1);
    assert_eq!(items[1], expected_item_2);
}

#[tokio::test]
async fn should_remove_item() {
    let (server, client, listener) = setup_test().await;
    let list_name = ListName::new(fixtures::random_string());
    let item_name = ItemName::new(fixtures::random_string());

    tokio::spawn(axum::serve(listener, server.into_router()).into_future());

    let list_id = client
        .create_list(list_name.clone())
        .await
        .expect("Error calling create list");

    let item_id = client
        .add_item(list_id, item_name.clone())
        .await
        .expect("Error running add item");

    let list = client
        .show_list(list_id)
        .await
        .expect("Error running show list");

    let items = list.items().cloned().collect::<Vec<Item>>();

    let expected_item = Item::new(item_id, item_name);
    assert_eq!(items.len(), 1);
    assert_eq!(items[0], expected_item);

    client
        .remove_item(list_id, item_id)
        .await
        .expect("Error running remove item");

    let list = client
        .show_list(list_id)
        .await
        .expect("Error running show list");

    assert!(list.items().next().is_none());
}

#[tokio::test]
async fn should_change_item_state() {
    let (server, client, listener) = setup_test().await;
    let list_name = ListName::new(fixtures::random_string());
    let item_name = ItemName::new(fixtures::random_string());
    let new_state = ItemState::NotFound;

    tokio::spawn(axum::serve(listener, server.into_router()).into_future());

    let list_id = client
        .create_list(list_name.clone())
        .await
        .expect("Error calling create list");

    let item_id = client
        .add_item(list_id, item_name.clone())
        .await
        .expect("Error running add item");

    let list = client
        .show_list(list_id)
        .await
        .expect("Error running show list");

    let items = list.items().cloned().collect::<Vec<Item>>();

    let expected_item = Item::new(item_id, item_name.clone());
    assert_eq!(items.len(), 1);
    assert_eq!(items[0], expected_item);

    client
        .change_item_state(list_id, item_id, new_state)
        .await
        .expect("Error running change item state");

    let list = client
        .show_list(list_id)
        .await
        .expect("Error running show list");

    let items = list.items().cloned().collect::<Vec<Item>>();

    let expected_item = Item::new(item_id, item_name).with_state(new_state);
    assert_eq!(items.len(), 1);
    assert_eq!(items[0], expected_item);
}

/// Setups the test
async fn setup_test() -> (
    AxumKikilistaServer<InMemoryListStorage, InMemoryTokenStorage>,
    ReqwestKikilistaClient,
    TcpListener,
) {
    let user = Username::new(fixtures::random_string());
    let token = Token::new(fixtures::random_string());
    let tokens = HashMap::from([(token.clone(), user)]);
    let list_storage = InMemoryListStorage::new();
    let token_storage = InMemoryTokenStorage::new().with_tokens(tokens);
    let server = AxumKikilistaServer::new(list_storage, token_storage);
    let listener = TcpListener::bind("0.0.0.0:0")
        .await
        .expect("Could not bind to address");
    let endpoint = listener.local_addr().expect("Could not get local address");
    let url =
        Url::from_str(&format!("http://{endpoint}")).expect("Could not parse URL from endpoint");
    let client = ReqwestKikilistaClient::new(url, token);
    (server, client, listener)
}
