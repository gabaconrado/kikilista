//! This test asserts the general behavior of the Kikilista API
//!
//! - Starts the Axum API in an available socket in the machine;
//! - Performs requests and assert the responses;
//! - All structures used are in memory;

use std::{collections::HashMap, future::IntoFuture};

use kikilista::{AxumKikilistaServer, InMemoryListStorage, InMemoryTokenStorage, Token, Username};
use reqwest::StatusCode;
use tokio::net::TcpListener;

/// Common test utilities
mod fixtures;

#[tokio::test]
async fn should_return_404_on_unknown_route() {
    let (server, listener, endpoint, ..) = setup_test().await;

    tokio::spawn(axum::serve(listener, server.into_router()).into_future());

    let response = reqwest::get(format!("{endpoint}/unknown"))
        .await
        .expect("Error performing request");

    assert_eq!(response.status(), StatusCode::NOT_FOUND);
}

#[tokio::test]
async fn should_return_405_on_unauthorized() {
    let (server, listener, endpoint, ..) = setup_test().await;

    tokio::spawn(axum::serve(listener, server.into_router()).into_future());

    let response = reqwest::get(format!("{endpoint}/list"))
        .await
        .expect("Error performing request");

    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);
}

#[tokio::test]
async fn should_return_401_on_invalid_method() {
    let (server, listener, endpoint, token, ..) = setup_test().await;

    tokio::spawn(axum::serve(listener, server.into_router()).into_future());

    let request = reqwest::Client::new()
        .put(format!("{endpoint}/list"))
        .header("auth-token", token.as_str())
        .build()
        .expect("Error building request");
    let response = reqwest::Client::new()
        .execute(request)
        .await
        .expect("Error performing request");

    assert_eq!(response.status(), StatusCode::METHOD_NOT_ALLOWED);
}

/// Setups the test
async fn setup_test() -> (
    AxumKikilistaServer<InMemoryListStorage, InMemoryTokenStorage>,
    TcpListener,
    String,
    Token,
) {
    let user = Username::new(fixtures::random_string());
    let token = Token::new(fixtures::random_string());
    let tokens = HashMap::from([(token.clone(), user)]);
    let list_storage = InMemoryListStorage::new();
    let token_storage = InMemoryTokenStorage::new().with_tokens(tokens);
    let server = AxumKikilistaServer::new(list_storage, token_storage);
    let listener = TcpListener::bind("0.0.0.0:0")
        .await
        .expect("Could not bind to address");
    let endpoint = listener.local_addr().expect("Could not get local address");
    (server, listener, format!("http://{endpoint}"), token)
}
