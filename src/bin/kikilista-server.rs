//! Kikilista Server
//!
//! An axum based REST API for the Kikilista system

#![deny(
    // RustC lints
    let_underscore_drop,
    missing_debug_implementations,
    unsafe_code,
    unused_import_braces,
    unused_results,
    // RustDoc lints
    clippy::missing_errors_doc,
    clippy::missing_panics_doc,
    missing_docs,
    rustdoc::broken_intra_doc_links,
    // Clippy lints
    clippy::cargo,
    clippy::complexity,
    clippy::correctness,
    clippy::pedantic,
    clippy::perf,
    clippy::style,
    clippy::suspicious,
    // Clippy restriction category lints
    clippy::as_conversions,
    clippy::clone_on_copy,
    clippy::clone_on_ref_ptr,
    clippy::else_if_without_else,
    clippy::exhaustive_enums,
    clippy::exhaustive_structs,
    clippy::exit,
    clippy::expect_used,
    clippy::if_then_some_else_none,
    clippy::indexing_slicing,
    clippy::let_underscore_must_use,
    clippy::mod_module_files,
    clippy::multiple_inherent_impl,
    clippy::panic,
    clippy::pattern_type_mismatch,
    clippy::possible_missing_comma,
    clippy::same_name_method,
    clippy::str_to_string,
    clippy::string_to_string,
    clippy::todo,
    clippy::unimplemented,
    clippy::unneeded_field_pattern,
    clippy::unreachable,
    clippy::unwrap_used,
)]
#![allow(clippy::multiple_crate_versions, clippy::module_name_repetitions)]

use std::{collections::HashMap, future::IntoFuture, net::SocketAddr, path::PathBuf};

use clap::Parser;
use kikilista::{
    AxumKikilistaServer, InMemoryTokenStorage, PersistentInMemoryListStorage, Token, Username,
};
use tokio::signal::unix::SignalKind;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let args = Cli::parse();

    setup_logging(args.debug);

    setup_data_directory(&args.data_dir)?;

    let tokens = read_tokens_file(args.tokens)?;
    let list_storage = PersistentInMemoryListStorage::new(args.data_dir.join("data.kikilista"))?;
    let tokens_storage = InMemoryTokenStorage::new().with_tokens(tokens);
    let router = AxumKikilistaServer::new(list_storage, tokens_storage).into_router();
    let listener = tokio::net::TcpListener::bind(&args.listen_socket).await?;
    let server_task = axum::serve(listener, router).into_future();
    let sigint_task = tokio::signal::ctrl_c();
    let mut sigterm_task = tokio::signal::unix::signal(SignalKind::terminate())?;

    tracing::info!("Starting server in socket: {}", args.listen_socket);
    tokio::select! {
        _ = server_task => tracing::info!("Server task shutting down"),
        _ = sigint_task => tracing::info!("Received sigint, gracefully shutting down the server"),
        _ = sigterm_task.recv() => tracing::info!("Received sigterm, gracefully shutting down the server"),
    };

    Ok(())
}

/// Setup the logging for the application
fn setup_logging(debug: bool) {
    let level = if debug {
        tracing::Level::DEBUG
    } else {
        tracing::Level::INFO
    };
    tracing_subscriber::fmt().with_max_level(level).init();
}

/// Reads the token file
fn read_tokens_file(path: PathBuf) -> anyhow::Result<HashMap<Token, Username>> {
    if path.is_file() {
        tracing::debug!("Reading token file {}", path.to_string_lossy());
        let tokens = config::Config::builder()
            .add_source(config::File::from(path))
            .build()?
            .try_deserialize::<HashMap<Token, Username>>()?;
        Ok(tokens)
    } else {
        tracing::warn!("Token file not found, server may not be accessible");
        Ok(HashMap::new())
    }
}

/// Setup the data directory
fn setup_data_directory(path: &PathBuf) -> anyhow::Result<()> {
    std::fs::create_dir_all(path)?;
    Ok(())
}

/// The CLI definition
#[derive(Debug, Parser)]
#[command(disable_help_subcommand(true))]
pub struct Cli {
    /// The debug flag, enables more detailing logging
    #[arg(long)]
    debug: bool,
    /// The application data directory
    #[arg(long, default_value = "./data")]
    data_dir: PathBuf,
    /// The tokens file to load
    #[arg(long, default_value = "./tokens.toml")]
    tokens: PathBuf,
    /// The socket to listen to
    #[arg(long, default_value = "0.0.0.0:9000")]
    listen_socket: SocketAddr,
}
