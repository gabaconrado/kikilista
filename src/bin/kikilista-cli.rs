//! Kikilista CLI
//!
//! A reqwest based REST client for the Kikilista system

#![deny(
    // RustC lints
    let_underscore_drop,
    missing_debug_implementations,
    unsafe_code,
    unused_import_braces,
    unused_results,
    // RustDoc lints
    clippy::missing_errors_doc,
    clippy::missing_panics_doc,
    missing_docs,
    rustdoc::broken_intra_doc_links,
    // Clippy lints
    clippy::cargo,
    clippy::complexity,
    clippy::correctness,
    clippy::pedantic,
    clippy::perf,
    clippy::style,
    clippy::suspicious,
    // Clippy restriction category lints
    clippy::as_conversions,
    clippy::clone_on_copy,
    clippy::clone_on_ref_ptr,
    clippy::else_if_without_else,
    clippy::exhaustive_enums,
    clippy::exhaustive_structs,
    clippy::exit,
    clippy::expect_used,
    clippy::if_then_some_else_none,
    clippy::indexing_slicing,
    clippy::let_underscore_must_use,
    clippy::mod_module_files,
    clippy::multiple_inherent_impl,
    clippy::panic,
    clippy::pattern_type_mismatch,
    clippy::possible_missing_comma,
    clippy::same_name_method,
    clippy::str_to_string,
    clippy::string_to_string,
    clippy::todo,
    clippy::unimplemented,
    clippy::unneeded_field_pattern,
    clippy::unreachable,
    clippy::unwrap_used,
)]
#![allow(clippy::multiple_crate_versions, clippy::module_name_repetitions)]
use std::fmt::Write;

use clap::Parser;
use kikilista::{ItemId, ItemName, ListId, ListName, ReqwestKikilistaClient, Token};
use ulid::Ulid;
use url::Url;

use cli::{Cli, Command};

/// Output for the CLI, stdout and stderr respectively
type CliOutput = (Option<String>, Option<String>);

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let args = Cli::parse();

    setup_logging(args.debug);

    let client = ReqwestKikilistaClient::new(args.server_address, Token::new(args.token));

    let (stdout, stderr) = match args.command {
        Command::CreateList { name } => create_list(&client, name).await?,
        Command::AllLists => all_lists(&client).await?,
        Command::ShowList { id } => show_list(&client, id).await?,
        Command::RemoveList { id } => remove_list(&client, id).await?,
        Command::AddItem { list_id, name } => add_item(&client, list_id, name).await?,
        Command::RemoveItem { list_id, item_id } => remove_item(&client, list_id, item_id).await?,
        Command::MarkItemAdded { list_id, item_id } => {
            mark_item_added(&client, list_id, item_id).await?
        }
        Command::MarkItemNotFound { list_id, item_id } => {
            mark_item_not_found(&client, list_id, item_id).await?
        }
        Command::MarkItemPicked { list_id, item_id } => {
            mark_item_picked(&client, list_id, item_id).await?
        }
    };

    if !args.quiet {
        if let Some(output) = stdout {
            println!("{output}");
        }
        if let Some(output) = stderr {
            eprintln!("{output}");
        }
    }

    Ok(())
}

/// Setup the logging for the application
fn setup_logging(debug: bool) {
    let level = if debug {
        tracing::Level::DEBUG
    } else {
        tracing::Level::INFO
    };
    tracing_subscriber::fmt().with_max_level(level).init();
}

async fn create_list(client: &ReqwestKikilistaClient, name: String) -> anyhow::Result<CliOutput> {
    let id = client.create_list(ListName::new(name)).await?;
    Ok((Some(format!("List was added with id {id}")), None))
}

async fn all_lists(client: &ReqwestKikilistaClient) -> anyhow::Result<CliOutput> {
    let lists = client.all_lists().await?;
    let output = if lists.is_empty() {
        "No lists saved".to_owned()
    } else {
        let mut output = String::new();
        for list in lists {
            write!(&mut output, "{}", utils::list_to_string(&list)?)?;
        }
        output
    };
    Ok((Some(output), None))
}

async fn show_list(client: &ReqwestKikilistaClient, id: Ulid) -> anyhow::Result<CliOutput> {
    let list = client.show_list(ListId::new(id)).await?;
    Ok((Some(utils::list_to_string(&list)?), None))
}

async fn remove_list(client: &ReqwestKikilistaClient, id: Ulid) -> anyhow::Result<CliOutput> {
    client.remove_list(ListId::new(id)).await?;
    Ok((Some(format!("List {id} removed")), None))
}

async fn add_item(
    client: &ReqwestKikilistaClient,
    list_id: Ulid,
    item_name: String,
) -> anyhow::Result<CliOutput> {
    let id = client
        .add_item(ListId::new(list_id), ItemName::new(item_name))
        .await?;
    Ok((Some(format!("Item added with id {}", id.as_ref())), None))
}

async fn remove_item(
    client: &ReqwestKikilistaClient,
    list_id: Ulid,
    item_id: Ulid,
) -> anyhow::Result<CliOutput> {
    client
        .remove_item(ListId::new(list_id), ItemId::new(item_id))
        .await?;
    Ok((Some(format!("Item {item_id} deleted")), None))
}

async fn mark_item_added(
    client: &ReqwestKikilistaClient,
    list_id: Ulid,
    item_id: Ulid,
) -> anyhow::Result<CliOutput> {
    client
        .change_item_state(
            ListId::new(list_id),
            ItemId::new(item_id),
            kikilista::ItemState::Added,
        )
        .await?;
    Ok((Some(format!("Item {item_id} marked as added")), None))
}

async fn mark_item_not_found(
    client: &ReqwestKikilistaClient,
    list_id: Ulid,
    item_id: Ulid,
) -> anyhow::Result<CliOutput> {
    client
        .change_item_state(
            ListId::new(list_id),
            ItemId::new(item_id),
            kikilista::ItemState::NotFound,
        )
        .await?;
    Ok((Some(format!("Item {item_id} marked as not found")), None))
}

async fn mark_item_picked(
    client: &ReqwestKikilistaClient,
    list_id: Ulid,
    item_id: Ulid,
) -> anyhow::Result<CliOutput> {
    client
        .change_item_state(
            ListId::new(list_id),
            ItemId::new(item_id),
            kikilista::ItemState::Picked,
        )
        .await?;
    Ok((Some(format!("Item {item_id} marked as picked")), None))
}

// Utility methods
mod utils {
    use std::fmt::Write;

    use kikilista::List;

    /// Transforms a list into a string
    pub fn list_to_string(list: &List) -> anyhow::Result<String> {
        let mut output = String::new();
        write!(&mut output, "\nList {} | {}", list.id(), list.name())?;
        for item in list.items() {
            write!(
                &mut output,
                "\n\t{} | {} | {}",
                item.id(),
                item.name(),
                item.state(),
            )?;
        }
        Ok(output)
    }
}

/// CLI definitions
mod cli {
    use super::{Parser, Ulid, Url};
    use clap::Subcommand;

    /// The CLI definition
    #[derive(Debug, Parser)]
    #[command(disable_help_subcommand(true))]
    pub struct Cli {
        /// The debug flag, enables more detailing logging
        #[arg(long)]
        pub debug: bool,
        /// The quiet flag, disables standard output and standard error
        #[arg(long)]
        pub quiet: bool,
        /// The authentication token to use in the requests
        #[arg(long, env = "KIKILISTA_CLI_AUTH_TOKEN")]
        pub token: String,
        /// The socket to listen to
        #[arg(long, env = "KIKILISTA_CLI_SERVER_ADDRESS")]
        pub server_address: Url,
        /// The command to the server
        #[command(subcommand)]
        pub command: Command,
    }

    #[derive(Debug, Subcommand)]
    pub enum Command {
        /// Create a new list
        CreateList {
            /// The name of the list
            name: String,
        },
        /// Return all lists
        AllLists,
        /// Return a list
        ShowList {
            /// The id of the list
            id: Ulid,
        },
        /// Remove a list
        RemoveList {
            /// The id of the list
            id: Ulid,
        },
        /// Add an item to a list
        AddItem {
            /// The id of the list
            list_id: Ulid,
            /// The name of the item
            name: String,
        },
        /// Remove an item from a list
        RemoveItem {
            /// The id of the list
            list_id: Ulid,
            /// The id of the item
            item_id: Ulid,
        },
        /// Mark an item from a list as added
        MarkItemAdded {
            /// The list id
            list_id: Ulid,
            /// The id of the item
            item_id: Ulid,
        },
        /// Mark an item from a list as not found
        MarkItemNotFound {
            /// The list id
            list_id: Ulid,
            /// The id of the item
            item_id: Ulid,
        },
        /// Mark an item from a list as picked
        MarkItemPicked {
            /// The list id
            list_id: Ulid,
            /// The id of the item
            item_id: Ulid,
        },
    }
}
