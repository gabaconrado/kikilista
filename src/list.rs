use self::types::{ItemId, ItemName, ItemState, List, ListId, ListName};

/// In memory implementation of [`ListStorage`]
pub mod in_memory;
/// A persistent in memory implementation of [`ListStorage`]
pub mod in_memory_persistent;
/// Types for the list functionality
pub mod types;

/// Abstracted functionality for list storage
pub trait ListStorage {
    /// Creates a new empty list, saves in storage and return its ID
    ///
    /// # Errors
    ///
    /// - Internal;
    fn new_list(&mut self, name: ListName) -> Result<ListId, ListStorageError>;

    /// Returns all saves lists
    ///
    /// # Errors
    ///
    /// - Internal;
    fn all_lists(&self) -> Result<Vec<List>, ListStorageError>;

    /// Gets a saved list from the storage
    ///
    /// Returns [Ok(None)] if not found
    ///
    /// # Errors
    ///
    /// - Internal;
    fn read_list(&self, list_id: ListId) -> Result<Option<List>, ListStorageError>;

    /// Removes a saved list from storage
    ///
    /// Returns [Ok(true)] if list was deleted, [Ok(false)] if list is not found
    ///
    /// # Errors
    ///
    /// - Internal;
    fn remove_list(&mut self, list_id: ListId) -> Result<bool, ListStorageError>;

    /// Creates a new item, add it to a list and returns its ID
    ///
    /// # Errors
    ///
    /// - List not found;
    fn new_item(&mut self, list_id: ListId, name: ItemName) -> Result<ItemId, ListStorageError>;

    /// Removes an item from the list
    ///
    /// Returns [Ok(true)] if list was deleted, [Ok(false)] if list is not found
    ///
    /// # Errors
    ///
    /// - Internal;
    fn remove_item(&mut self, list_id: ListId, item_id: ItemId) -> Result<bool, ListStorageError>;

    /// Updates the state of an item in a list
    ///
    /// # Errors
    ///
    /// - Internal;
    fn update_item(
        &mut self,
        list_id: ListId,
        item_id: ItemId,
        new_state: ItemState,
    ) -> Result<(), ListStorageError>;
}

/// Possible errors of the list storage
#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
#[non_exhaustive]
pub enum ListStorageError {
    /// Requested list was not found in storage
    #[error("List was not found in storage")]
    ListNotFound,
    /// Internal error
    #[error("Internal storage error: {0}")]
    Internal(String),
}
