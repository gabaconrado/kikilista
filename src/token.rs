use serde::{Deserialize, Serialize};

/// In memory implementation of [`TokenStorage`]
pub mod in_memory;

/// The name of an user holding a [`Token`] in the system
#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize, shrinkwraprs::Shrinkwrap)]
pub struct Username(String);

impl Username {
    /// Create a new [`Username`]
    pub fn new<IntoString>(value: IntoString) -> Self
    where
        IntoString: Into<String>,
    {
        Self(value.into())
    }
}

/// The value of an authentication token
#[derive(Debug, Clone, Hash, PartialEq, Eq, Deserialize, Serialize, shrinkwraprs::Shrinkwrap)]
pub struct Token(String);

impl Token {
    /// Create a new [`Token`]
    pub fn new<IntoString>(value: IntoString) -> Self
    where
        IntoString: Into<String>,
    {
        Self(value.into())
    }
}

/// Behavior expected of token storage
pub trait TokenStorage {
    /// Gets the owned of a token in storage, if it exists
    ///
    /// # Errors
    ///
    /// - Internal
    fn owner(&self, token: &Token) -> Result<Option<Username>, TokenStorageError>;
}

/// Errors thrown by the [`TokenStorage`]
#[derive(Debug, thiserror::Error)]
pub enum TokenStorageError {
    /// Internal error
    #[error("Internal error: {0}")]
    Internal(String),
}
