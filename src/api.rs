use serde::{Deserialize, Serialize};

use crate::{
    list::types::{ItemState, List},
    token::Token,
    ItemId, ItemName, ListId, ListName,
};

/// [`axum`] based implementation of the API server
pub(crate) mod axum_server;
/// [`reqwest`] based implementation of the API Client
pub(crate) mod reqwest_client;

/// The full API of the Kikilista system
pub trait KikilistaApi {
    /// Authenticates a request
    ///
    /// Returns [Ok(true)] if credentials are valid, [Ok(false)] otherwise
    ///
    /// # Errors
    ///
    /// - Internal;
    fn authenticate(&self, token: &Token) -> Result<bool, KikilistaApiError>;

    /// Creates a new empty list, saves in storage and return its ID
    ///
    /// # Errors
    ///
    /// - Internal;
    fn new_list(&self, request: NewListRequest) -> Result<NewListResponse, KikilistaApiError>;

    /// Returns all saves lists
    ///
    /// # Errors
    ///
    /// - Internal;
    fn all_lists(&self, request: AllListsRequest) -> Result<AllListsResponse, KikilistaApiError>;

    /// Gets a saved list from the storage
    ///
    /// Returns [Ok(None)] if not found
    ///
    /// # Errors
    ///
    /// - List not found;
    /// - Internal;
    fn show_list(&self, request: ShowListRequest) -> Result<ShowListResponse, KikilistaApiError>;

    /// Removes a saved list from storage
    ///
    /// Returns [Ok(true)] if list was deleted, [Ok(false)] if list is not found
    ///
    /// # Errors
    ///
    /// - List not found;
    /// - Internal;
    fn remove_list(
        &self,
        request: RemoveListRequest,
    ) -> Result<RemoveListResponse, KikilistaApiError>;

    /// Creates a new item, add it to a list and returns its ID
    ///
    /// # Errors
    ///
    /// - List not found;
    /// - Internal;
    fn new_item(&self, request: NewItemRequest) -> Result<NewItemResponse, KikilistaApiError>;

    /// Removes an item from the list
    ///
    /// Returns [Ok(true)] if list was deleted, [Ok(false)] if list is not found
    ///
    /// # Errors
    ///
    /// - List not found;
    /// - Item not found;
    /// - Internal;
    fn remove_item(
        &self,
        request: RemoveItemRequest,
    ) -> Result<RemoveItemResponse, KikilistaApiError>;

    /// Updates the state of an item in a list
    ///
    /// # Errors
    ///
    /// - Item not found;
    /// - Internal;
    fn update_item(
        &self,
        request: ChangeItemStateRequest,
    ) -> Result<ChangeItemStateResponse, KikilistaApiError>;
}

/// Possible errors of the Kikilista API
#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
#[non_exhaustive]
pub enum KikilistaApiError {
    /// Requested list was not found in storage
    #[error("List was not found")]
    ListNotFound,
    /// Requested item was not found in storage
    #[error("Item was not found")]
    ItemNotFound,
    #[error("Internal error: {0}")]
    Internal(String),
}

#[derive(Debug, Serialize, Deserialize)]
pub struct NewListRequest {
    pub name: ListName,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct NewListResponse {
    pub id: ListId,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AllListsRequest {}

#[derive(Debug, Serialize, Deserialize)]
pub struct AllListsResponse {
    pub lists: Vec<List>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ShowListRequest {
    pub id: ListId,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ShowListResponse {
    pub list: List,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RemoveListRequest {
    pub id: ListId,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RemoveListResponse {}

#[derive(Debug, Serialize, Deserialize)]
pub struct NewItemRequest {
    pub list_id: ListId,
    pub name: ItemName,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct NewItemResponse {
    pub id: ItemId,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RemoveItemRequest {
    pub list_id: ListId,
    pub item_id: ItemId,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RemoveItemResponse {}

#[derive(Debug, Serialize, Deserialize)]
pub struct ChangeItemStateRequest {
    pub list_id: ListId,
    pub item_id: ItemId,
    pub state: ItemState,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ChangeItemStateResponse {}
