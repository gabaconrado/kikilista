use std::collections::HashMap;

use crate::Username;

use super::{Token, TokenStorage, TokenStorageError};

/// An in-memory implementation of Token Storage
#[derive(Debug, Default, Clone)]
pub struct InMemoryTokenStorage {
    tokens: HashMap<Token, Username>,
}

impl InMemoryTokenStorage {
    /// Creates a new [`InMemoryTokenStorage`]
    #[must_use]
    pub fn new() -> Self {
        Self {
            tokens: HashMap::new(),
        }
    }

    /// Replaces the internal token object with the passed one
    #[must_use]
    pub fn with_tokens<IntoTokens>(self, tokens: IntoTokens) -> Self
    where
        IntoTokens: Into<HashMap<Token, Username>>,
    {
        Self {
            tokens: tokens.into(),
        }
    }

    /// Returns an iterator to the stored tokens
    #[must_use]
    pub fn iter(&self) -> std::collections::hash_map::Iter<Token, Username> {
        self.tokens.iter()
    }
}

impl TokenStorage for InMemoryTokenStorage {
    /// Retrieves a token from the storage
    fn owner(&self, token: &Token) -> Result<Option<Username>, TokenStorageError> {
        Ok(self.tokens.get(token).cloned())
    }
}
