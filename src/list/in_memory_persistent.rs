use std::{
    collections::HashMap,
    fs::{self, File},
    io::{self, Read},
    path::PathBuf,
};

use serde_json::json;

use crate::{list::in_memory::InMemoryListStorage, ItemId, ItemName, ListId, ListName};

use super::{
    types::{ItemState, List},
    ListStorageError,
};

/// An in memory List Storage that persists to disk
///
/// # Notes
///
/// - If [Drop] is not called, data will not be persisted;
#[derive(Debug, Default, Clone)]
pub struct PersistentInMemoryListStorage {
    /// The in-memory storage
    in_memory_storage: InMemoryListStorage,
    /// The path to the file of persistence
    data_path: PathBuf,
}

impl PersistentInMemoryListStorage {
    /// Creates a new [`PersistentInMemoryListStorage`]
    ///
    /// This method will load the persisted data in the provided data path
    ///
    /// It creates the data if it does not exist
    ///
    /// # Notes
    ///
    /// - The whole file will be loaded to memory;
    ///
    /// # Errors
    ///
    /// - Failure reading the path;
    pub fn new(data_path: PathBuf) -> Result<Self, io::Error> {
        let mut file_content = Vec::new();
        {
            let mut file = File::options()
                .create(true)
                .write(true)
                .read(true)
                .open(&data_path)?;
            let _bytes_read = file.read_to_end(&mut file_content);
        }
        let in_memory_storage = if file_content.is_empty() {
            InMemoryListStorage::new()
        } else {
            let lists = serde_json::from_slice::<Vec<List>>(&file_content)?
                .into_iter()
                .map(|l| (l.id(), l))
                .collect::<HashMap<ListId, List>>();
            InMemoryListStorage::new().with_lists(lists)
        };
        Ok(Self {
            in_memory_storage,
            data_path,
        })
    }
}

impl Drop for PersistentInMemoryListStorage {
    /// Persists the data according to the configured type
    ///
    /// # Notes
    ///
    /// - Errors are ignored;
    fn drop(&mut self) {
        let contents = self
            .in_memory_storage
            .iter()
            .map(|(_, list)| list)
            .cloned()
            .collect::<Vec<List>>();
        let serialized = json!(contents);
        let _written = serde_json::to_vec(&serialized)
            .ok()
            .map(|serialized_bytes| fs::write(&self.data_path, serialized_bytes).ok());
    }
}

impl crate::list::ListStorage for PersistentInMemoryListStorage {
    fn new_list(&mut self, name: ListName) -> Result<ListId, ListStorageError> {
        self.in_memory_storage.new_list(name)
    }

    fn read_list(&self, list_id: ListId) -> Result<Option<List>, ListStorageError> {
        self.in_memory_storage.read_list(list_id)
    }

    fn remove_list(&mut self, list_id: ListId) -> Result<bool, ListStorageError> {
        self.in_memory_storage.remove_list(list_id)
    }

    fn new_item(&mut self, list_id: ListId, name: ItemName) -> Result<ItemId, ListStorageError> {
        self.in_memory_storage.new_item(list_id, name)
    }

    fn remove_item(&mut self, list_id: ListId, item_id: ItemId) -> Result<bool, ListStorageError> {
        self.in_memory_storage.remove_item(list_id, item_id)
    }

    fn update_item(
        &mut self,
        list_id: ListId,
        item_id: ItemId,
        new_state: ItemState,
    ) -> Result<(), ListStorageError> {
        self.in_memory_storage
            .update_item(list_id, item_id, new_state)
    }

    fn all_lists(&self) -> Result<Vec<List>, ListStorageError> {
        self.in_memory_storage.all_lists()
    }
}
