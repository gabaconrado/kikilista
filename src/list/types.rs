use serde::{Deserialize, Serialize};
use ulid::Ulid;

/// A shopping list
#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct List {
    /// Id of the list
    id: ListId,
    /// Name of the list
    name: ListName,
    /// An array of items in the list
    items: Vec<Item>,
}

impl List {
    /// Create a new [`List`]
    #[must_use]
    pub fn new(id: ListId, name: ListName) -> Self {
        Self {
            id,
            name,
            items: Vec::new(),
        }
    }

    /// List identifier
    #[must_use]
    pub fn id(&self) -> ListId {
        self.id
    }

    /// List name
    #[must_use]
    pub fn name(&self) -> &ListName {
        &self.name
    }

    /// Replaces the internal item list with the passed in
    #[must_use]
    pub fn set_items<IntoItems>(self, items: IntoItems) -> Self
    where
        IntoItems: Into<Vec<Item>>,
    {
        Self {
            items: items.into(),
            ..self
        }
    }

    /// Gets an iterator over the items of the list
    pub fn items(&self) -> std::slice::Iter<'_, Item> {
        self.items.iter()
    }

    /// Adds a new item to the list
    pub fn add_item(&mut self, item: Item) {
        self.items.push(item);
    }

    /// Remove an item from the list
    ///
    /// Returns the removed item if it was present,[`None`] otherwise
    pub fn remove_item(&mut self, item_id: ItemId) -> Option<Item> {
        self.items
            .iter()
            .position(|item| item.id == item_id)
            .map(|index| self.items.remove(index))
    }

    /// Update an item state
    pub fn update_item_state(&mut self, item_id: ItemId, state: ItemState) {
        let _updated = self
            .items
            .iter()
            .position(|item| item.id == item_id)
            .and_then(|index| self.items.get_mut(index))
            .map(|item| item.set_state(state));
    }
}

/// The name of a list
#[derive(Debug, Clone, PartialEq, Eq, Hash, shrinkwraprs::Shrinkwrap, Serialize, Deserialize)]
pub struct ListName(String);

impl ListName {
    /// Create a new [`ListName`]
    #[must_use]
    pub fn new<IntoString>(value: IntoString) -> Self
    where
        IntoString: Into<String>,
    {
        Self(value.into())
    }
}

impl std::fmt::Display for ListName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// The ID of a list
#[derive(
    Debug,
    Clone,
    Copy,
    PartialEq,
    Eq,
    Hash,
    shrinkwraprs::Shrinkwrap,
    Serialize,
    Deserialize,
    PartialOrd,
    Ord,
)]
pub struct ListId(Ulid);

impl ListId {
    /// Create a new [`ListId`]
    #[must_use]
    pub fn new<IntoUlid>(value: IntoUlid) -> Self
    where
        IntoUlid: Into<Ulid>,
    {
        Self(value.into())
    }

    /// Creates a new globally unique random id
    #[must_use]
    pub fn random() -> Self {
        Self(Ulid::new())
    }
}

impl std::fmt::Display for ListId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// An item in a shopping list
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Item {
    /// The id of the item
    id: ItemId,
    /// The name of the item
    name: ItemName,
    /// State of the item
    state: ItemState,
}

impl Item {
    /// Create a new [`Item`]
    #[must_use]
    pub fn new(id: ItemId, name: ItemName) -> Self {
        Self {
            id,
            name,
            state: ItemState::Added,
        }
    }

    /// Set the state of the [`Item`]
    #[must_use]
    pub fn with_state(self, state: ItemState) -> Self {
        Self { state, ..self }
    }

    /// Update the state of an [`Item`]
    pub fn set_state(&mut self, new_state: ItemState) {
        self.state = new_state;
    }

    /// The identifier of the [`Item`]
    #[must_use]
    pub fn id(&self) -> ItemId {
        self.id
    }

    /// The name of the [`Item`]
    #[must_use]
    pub fn name(&self) -> &ItemName {
        &self.name
    }

    /// The state of the [`Item`]
    #[must_use]
    pub fn state(&self) -> &ItemState {
        &self.state
    }
}

/// The name of an item
#[derive(Debug, Clone, PartialEq, Eq, Hash, shrinkwraprs::Shrinkwrap, Serialize, Deserialize)]
pub struct ItemName(String);

impl ItemName {
    /// Create a new [`ItemName`]
    #[must_use]
    pub fn new<IntoString>(value: IntoString) -> Self
    where
        IntoString: Into<String>,
    {
        Self(value.into())
    }
}

impl std::fmt::Display for ItemName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// The ID of an item
#[derive(
    Debug,
    Clone,
    Copy,
    PartialEq,
    Eq,
    Hash,
    shrinkwraprs::Shrinkwrap,
    Serialize,
    Deserialize,
    PartialOrd,
    Ord,
)]
pub struct ItemId(Ulid);

impl ItemId {
    /// Create a new [`ListId`]
    #[must_use]
    pub fn new<IntoUlid>(value: IntoUlid) -> Self
    where
        IntoUlid: Into<Ulid>,
    {
        Self(value.into())
    }

    /// Creates a new globally unique random id
    #[must_use]
    pub fn random() -> Self {
        Self(Ulid::new())
    }
}

impl std::fmt::Display for ItemId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// Possible states of an item in a list
#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
#[non_exhaustive]
pub enum ItemState {
    /// Item is added to the list
    Added,
    /// Item was not found during shopping
    NotFound,
    /// Item was picked
    Picked,
}

impl std::fmt::Display for ItemState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match *self {
            ItemState::Added => write!(f, "Added"),
            ItemState::NotFound => write!(f, "Not found"),
            ItemState::Picked => write!(f, "Picked"),
        }
    }
}
