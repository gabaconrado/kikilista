use std::collections::{hash_map::Entry, HashMap};

use crate::list::types::Item;

use super::{
    types::{ItemId, ItemName, List, ListId, ListName},
    ListStorage, ListStorageError,
};

/// An in-memory implementation of List Storage
///
/// Reads and writes have time complexity O(1), space complexity is O(n)
///
/// # Example
#[derive(Debug, Default, Clone)]
pub struct InMemoryListStorage {
    lists: HashMap<ListId, List>,
}

impl InMemoryListStorage {
    /// Creates a new empty [`InMemoryListStorage`]
    #[must_use]
    pub fn new() -> Self {
        Self {
            lists: HashMap::new(),
        }
    }

    /// Replaces the saved lists with the passed object
    #[must_use]
    pub fn with_lists<IntoLists>(self, lists: IntoLists) -> Self
    where
        IntoLists: Into<HashMap<ListId, List>>,
    {
        Self {
            lists: lists.into(),
        }
    }

    /// Returns an iterator over the stored lists
    #[must_use]
    pub fn iter(&self) -> std::collections::hash_map::Iter<ListId, List> {
        self.lists.iter()
    }
}

impl ListStorage for InMemoryListStorage {
    fn new_list(&mut self, name: ListName) -> Result<ListId, ListStorageError> {
        let id = ListId::random();
        let list = List::new(id, name);
        let _old = self.lists.insert(id, list);
        Ok(id)
    }

    fn new_item(&mut self, list_id: ListId, name: ItemName) -> Result<ItemId, ListStorageError> {
        match self.lists.entry(list_id) {
            Entry::Occupied(mut list) => {
                let id = ItemId::random();
                let item = Item::new(id, name);
                list.get_mut().add_item(item);
                Ok(id)
            }
            Entry::Vacant(_) => Err(ListStorageError::ListNotFound),
        }
    }

    fn all_lists(&self) -> Result<Vec<List>, ListStorageError> {
        Ok(self.lists.values().cloned().collect())
    }

    fn read_list(&self, list_id: ListId) -> Result<Option<List>, ListStorageError> {
        Ok(self.lists.get(&list_id).cloned())
    }

    fn remove_list(&mut self, list_id: ListId) -> Result<bool, ListStorageError> {
        Ok(self.lists.remove(&list_id).is_some())
    }

    fn remove_item(&mut self, list_id: ListId, item_id: ItemId) -> Result<bool, ListStorageError> {
        let removed = self
            .lists
            .get_mut(&list_id)
            .map(|list| list.remove_item(item_id));
        Ok(removed.is_some())
    }

    fn update_item(
        &mut self,
        list_id: ListId,
        item_id: ItemId,
        new_state: super::types::ItemState,
    ) -> Result<(), ListStorageError> {
        let _updated = self
            .lists
            .get_mut(&list_id)
            .map(|list| list.update_item_state(item_id, new_state));
        Ok(())
    }
}
