//! Kikilista library crate
//!
//! Provides functionality for the Kikilista API and utilities

/*** Lint rules ***/

#![deny(
    // RustC lints
    let_underscore_drop,
    missing_debug_implementations,
    unsafe_code,
    unused_import_braces,
    unused_results,
    // RustDoc lints
    clippy::missing_errors_doc,
    clippy::missing_panics_doc,
    missing_docs,
    rustdoc::broken_intra_doc_links,
    // Clippy lints
    clippy::cargo,
    clippy::complexity,
    clippy::correctness,
    clippy::pedantic,
    clippy::perf,
    clippy::style,
    clippy::suspicious,
    // Clippy restriction category lints
    clippy::as_conversions,
    clippy::clone_on_copy,
    clippy::clone_on_ref_ptr,
    clippy::else_if_without_else,
    clippy::exhaustive_enums,
    clippy::exhaustive_structs,
    clippy::exit,
    clippy::expect_used,
    clippy::if_then_some_else_none,
    clippy::indexing_slicing,
    clippy::let_underscore_must_use,
    clippy::mod_module_files,
    clippy::multiple_inherent_impl,
    clippy::panic,
    clippy::pattern_type_mismatch,
    clippy::possible_missing_comma,
    clippy::same_name_method,
    clippy::str_to_string,
    clippy::string_to_string,
    clippy::todo,
    clippy::unimplemented,
    clippy::unneeded_field_pattern,
    clippy::unreachable,
    clippy::unwrap_used,
)]
#![allow(clippy::multiple_crate_versions, clippy::module_name_repetitions)]

/*** Submodules ***/

/// The api implementation
mod api;
/// List functionality implementation
mod list;
/// Authentication token functionality implementation
mod token;

/*** Re-exports ***/

pub use self::{
    api::{
        axum_server::{AxumKikilistaServer, AxumKikilistaServerError},
        reqwest_client::{ReqwestKikilistaClient, ReqwestKikilistaClientError},
    },
    list::{
        in_memory::InMemoryListStorage,
        in_memory_persistent::PersistentInMemoryListStorage,
        types::{Item, ItemId, ItemName, ItemState, List, ListId, ListName},
    },
    token::{in_memory::InMemoryTokenStorage, Token, Username},
};
