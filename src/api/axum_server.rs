use axum::{routing, Router};
use std::sync::{Arc, Mutex};

use crate::{list::ListStorage, token::TokenStorage};

use super::{KikilistaApi, KikilistaApiError};

/// Implementation of the Kikilista api
mod api_impl;
/// Handlers implementation
mod handlers;
/// Middlewares implementation
mod middleware;

/// An axum-based server of the Kikilista Api
#[derive(Debug)]
pub struct AxumKikilistaServer<L, T> {
    /// List storage object
    list_storage: Mutex<L>,
    /// Token storage object
    token_storage: Mutex<T>,
}

impl<L, T> AxumKikilistaServer<L, T> {
    /// Create a new [`AxumKikilistaServer`] with default values
    pub fn new(list_storage: L, token_storage: T) -> Self {
        Self {
            list_storage: Mutex::new(list_storage),
            token_storage: Mutex::new(token_storage),
        }
    }
}

impl<L, T> AxumKikilistaServer<L, T>
where
    L: ListStorage + Send + Sync + 'static,
    T: TokenStorage + Send + Sync + 'static,
{
    /// Build an [`axum::Router`] from the internal object
    pub fn into_router(self) -> axum::Router {
        let storage = Arc::new(self);
        let list_routes = Router::new()
            .route("/", routing::post(handlers::create_list))
            .route("/", routing::get(handlers::all_lists))
            .route("/:list_id", routing::get(handlers::show_list))
            .route("/:list_id", routing::delete(handlers::remove_list))
            .route("/:list_id", routing::put(handlers::add_item))
            .route("/:list_id/:item_id", routing::delete(handlers::remove_item))
            .route(
                "/:list_id/:item_id",
                routing::put(handlers::change_item_state),
            );
        Router::new()
            .nest("/list", list_routes)
            .layer(middleware::trace_middleware())
            .route_layer(axum::middleware::from_fn_with_state(
                Arc::clone(&storage),
                middleware::token_middleware,
            ))
            .with_state(storage)
            .fallback(handlers::not_found)
    }
}

/// Errors returned by the [`AxumKikilistaServer`]
#[derive(Debug, thiserror::Error)]
#[non_exhaustive]
pub enum AxumKikilistaServerError {
    /// Invalid credentials on request
    #[error("Unauthenticated request")]
    Unauthenticated,
    /// Resource requested was not found
    #[error("Not found resource")]
    NotFound,
    /// Internal error
    #[error("Internal error")]
    Internal,
}

impl From<KikilistaApiError> for AxumKikilistaServerError {
    fn from(value: KikilistaApiError) -> Self {
        match value {
            KikilistaApiError::ListNotFound | KikilistaApiError::ItemNotFound => Self::NotFound,
            KikilistaApiError::Internal(_) => Self::Internal,
        }
    }
}
