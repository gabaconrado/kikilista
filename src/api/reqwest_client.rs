use reqwest::{Client, RequestBuilder, StatusCode, Url};
use serde::Deserialize;

use crate::{
    api::{NewListRequest, NewListResponse},
    list::types::{ItemState, List},
    ItemId, ItemName, ListId, ListName, Token,
};

use super::{
    AllListsRequest, AllListsResponse, ChangeItemStateRequest, ChangeItemStateResponse,
    NewItemRequest, NewItemResponse, RemoveItemRequest, RemoveItemResponse, RemoveListRequest,
    RemoveListResponse, ShowListRequest, ShowListResponse,
};

/// A reqwest-based client of the Kikilista Api
#[derive(Debug)]
pub struct ReqwestKikilistaClient {
    /// The Kikilista server endpoint
    endpoint: Url,
    /// Authentication token to be used
    token: Token,
}

impl ReqwestKikilistaClient {
    /// Create a new [`ReqwestKikilistaClient`]
    #[must_use]
    pub fn new(endpoint: Url, token: Token) -> Self {
        Self { endpoint, token }
    }

    /// Calls the server endpoint to create a list
    ///
    /// # Errors
    ///
    /// - Building the request;
    /// - Sending the request;
    /// - Error response from server;
    pub async fn create_list(&self, name: ListName) -> Result<ListId, ReqwestKikilistaClientError> {
        let url = self.endpoint.join("list")?;
        let client = Client::builder().build()?;
        let response = self
            .send_request::<NewListResponse>(client.post(url).json(&NewListRequest { name }))
            .await?;
        Ok(response.id)
    }

    /// Calls the server endpoint to return all list
    ///
    /// # Errors
    ///
    /// - Building the request;
    /// - Sending the request;
    /// - Error response from server;
    pub async fn all_lists(&self) -> Result<Vec<List>, ReqwestKikilistaClientError> {
        let url = self.endpoint.join("list")?;
        let client = Client::builder().build()?;
        let response = self
            .send_request::<AllListsResponse>(client.get(url).json(&AllListsRequest {}))
            .await?;
        Ok(response.lists)
    }

    /// Calls the server endpoint to show a list
    ///
    /// # Errors
    ///
    /// - Building the request;
    /// - Sending the request;
    /// - Error response from server;
    pub async fn show_list(&self, id: ListId) -> Result<List, ReqwestKikilistaClientError> {
        let path = format!("list/{id}");
        let url = self.endpoint.join(&path)?;
        let client = Client::builder().build()?;
        let response = self
            .send_request::<ShowListResponse>(client.get(url).json(&ShowListRequest { id }))
            .await?;
        Ok(response.list)
    }

    /// Calls the server endpoint to remove a list
    ///
    /// # Errors
    ///
    /// - Building the request;
    /// - Sending the request;
    /// - Error response from server;
    pub async fn remove_list(&self, id: ListId) -> Result<(), ReqwestKikilistaClientError> {
        let path = format!("list/{id}");
        let url = self.endpoint.join(&path)?;
        let client = Client::builder().build()?;
        let _response = self
            .send_request::<RemoveListResponse>(client.delete(url).json(&RemoveListRequest { id }))
            .await?;
        Ok(())
    }

    /// Calls the server endpoint to add an item to a list
    ///
    /// # Errors
    ///
    /// - Building the request;
    /// - Sending the request;
    /// - Error response from server;
    pub async fn add_item(
        &self,
        list_id: ListId,
        name: ItemName,
    ) -> Result<ItemId, ReqwestKikilistaClientError> {
        let path = format!("list/{list_id}");
        let url = self.endpoint.join(&path)?;
        let client = Client::builder().build()?;
        let response = self
            .send_request::<NewItemResponse>(
                client.put(url).json(&NewItemRequest { list_id, name }),
            )
            .await?;
        Ok(response.id)
    }

    /// Calls the server endpoint to remove an item from a list
    ///
    /// # Errors
    ///
    /// - Building the request;
    /// - Sending the request;
    /// - Error response from server;
    pub async fn remove_item(
        &self,
        list_id: ListId,
        item_id: ItemId,
    ) -> Result<(), ReqwestKikilistaClientError> {
        let path = format!("list/{list_id}/{item_id}");
        let url = self.endpoint.join(&path)?;
        let client = Client::builder().build()?;
        let _response = self
            .send_request::<RemoveItemResponse>(
                client
                    .delete(url)
                    .json(&RemoveItemRequest { list_id, item_id }),
            )
            .await?;
        Ok(())
    }

    /// Calls the server endpoint to change the state of an item
    ///
    /// # Errors
    ///
    /// - Building the request;
    /// - Sending the request;
    /// - Error response from server;
    pub async fn change_item_state(
        &self,
        list_id: ListId,
        item_id: ItemId,
        state: ItemState,
    ) -> Result<(), ReqwestKikilistaClientError> {
        let path = format!("list/{list_id}/{item_id}");
        let url = self.endpoint.join(&path)?;
        let client = Client::builder().build()?;
        let _response = self
            .send_request::<ChangeItemStateResponse>(client.put(url).json(
                &ChangeItemStateRequest {
                    list_id,
                    item_id,
                    state,
                },
            ))
            .await?;
        Ok(())
    }

    /// Sends a request to the server and parses the response
    async fn send_request<Res>(
        &self,
        request: RequestBuilder,
    ) -> Result<Res, ReqwestKikilistaClientError>
    where
        for<'a> Res: Deserialize<'a>,
    {
        let request = request.header("auth-token", self.token.as_str()).build()?;
        let client = reqwest::Client::builder().build()?;
        let raw_response = client.execute(request).await?;
        match raw_response.status() {
            status if status.is_success() => Ok(raw_response.json().await?),
            StatusCode::UNAUTHORIZED => Err(ReqwestKikilistaClientError::InvalidCredentials),
            StatusCode::NOT_FOUND => Err(ReqwestKikilistaClientError::NotFound),
            StatusCode::BAD_REQUEST => Err(ReqwestKikilistaClientError::BadRequest),
            _other => Err(ReqwestKikilistaClientError::InternalServerError),
        }
    }
}

/// All errors returned by [`ReqwestKikilistaClient`]
#[derive(Debug, thiserror::Error)]
#[non_exhaustive]
pub enum ReqwestKikilistaClientError {
    /// Error internal to the [`reqwest`] library
    #[error(transparent)]
    Reqwest(#[from] reqwest::Error),
    /// Error parsing URLs
    #[error(transparent)]
    Url(#[from] url::ParseError),
    /// Resource not found
    #[error("Requested resource not found")]
    NotFound,
    /// Invalid request credentials
    #[error("Invalid request credentials")]
    InvalidCredentials,
    /// Bad request
    #[error("Bad request")]
    BadRequest,
    /// Internal server error
    #[error("Internal server error")]
    InternalServerError,
}
