use std::sync::Arc;

use axum::{
    extract::{Path, State},
    Json,
};

use crate::{
    api::{
        AllListsRequest, AllListsResponse, ChangeItemStateRequest, ChangeItemStateResponse,
        KikilistaApi, NewItemRequest, NewItemResponse, NewListRequest, NewListResponse,
        RemoveItemRequest, RemoveItemResponse, RemoveListRequest, RemoveListResponse,
        ShowListRequest, ShowListResponse,
    },
    ItemId, ListId,
};

use self::error::HandlerError;

pub(super) async fn create_list<Api>(
    State(api): State<Arc<Api>>,
    Json(request): Json<NewListRequest>,
) -> Result<Json<NewListResponse>, HandlerError>
where
    Api: KikilistaApi,
{
    let response = api.new_list(request)?;
    Ok(Json(response))
}

pub async fn all_lists<Api>(
    State(api): State<Arc<Api>>,
    Json(request): Json<AllListsRequest>,
) -> Result<Json<AllListsResponse>, HandlerError>
where
    Api: KikilistaApi,
{
    let response = api.all_lists(request)?;
    Ok(Json(response))
}

pub async fn show_list<Api>(
    State(api): State<Arc<Api>>,
    Path(list_id): Path<ListId>,
) -> Result<Json<ShowListResponse>, HandlerError>
where
    Api: KikilistaApi,
{
    let response = api.show_list(ShowListRequest { id: list_id })?;
    Ok(Json(response))
}

pub async fn remove_list<Api>(
    State(api): State<Arc<Api>>,
    Path(list_id): Path<ListId>,
) -> Result<Json<RemoveListResponse>, HandlerError>
where
    Api: KikilistaApi,
{
    let response = api.remove_list(RemoveListRequest { id: list_id })?;
    Ok(Json(response))
}

pub async fn add_item<Api>(
    State(api): State<Arc<Api>>,
    Path(list_id): Path<ListId>,
    Json(request): Json<NewItemRequest>,
) -> Result<Json<NewItemResponse>, HandlerError>
where
    Api: KikilistaApi,
{
    if list_id != request.list_id {
        return Err(HandlerError::InvalidRequest(
            "List ID in request is inconsistent with URL".to_owned(),
        ));
    }
    let response = api.new_item(NewItemRequest {
        list_id,
        name: request.name,
    })?;
    Ok(Json(response))
}

pub async fn remove_item<Api>(
    State(api): State<Arc<Api>>,
    Path((list_id, item_id)): Path<(ListId, ItemId)>,
) -> Result<Json<RemoveItemResponse>, HandlerError>
where
    Api: KikilistaApi,
{
    let response = api.remove_item(RemoveItemRequest { list_id, item_id })?;
    Ok(Json(response))
}

pub async fn change_item_state<Api>(
    State(api): State<Arc<Api>>,
    Path((list_id, item_id)): Path<(ListId, ItemId)>,
    Json(request): Json<ChangeItemStateRequest>,
) -> Result<Json<ChangeItemStateResponse>, HandlerError>
where
    Api: KikilistaApi,
{
    if list_id != request.list_id {
        return Err(HandlerError::InvalidRequest(
            "List ID in request is inconsistent with URL".to_owned(),
        ));
    }
    if item_id != request.item_id {
        return Err(HandlerError::InvalidRequest(
            "Item ID in request is inconsistent with URL".to_owned(),
        ));
    }
    let response = api.update_item(ChangeItemStateRequest {
        list_id,
        item_id,
        state: request.state,
    })?;
    Ok(Json(response))
}

pub async fn not_found() -> HandlerError {
    HandlerError::NotFound("Nothing to see here".to_owned())
}

/// Error for the handlers
pub(super) mod error {
    use axum::{http::StatusCode, response::IntoResponse};

    use crate::api::KikilistaApiError;

    #[derive(Debug, thiserror::Error)]
    pub enum HandlerError {
        #[error("Unauthenticated request")]
        Unauthenticated,
        #[error("Not found resource: {0}")]
        NotFound(String),
        #[error("Internal error: {0}")]
        Internal(String),
        #[error("Invalid request: {0}")]
        InvalidRequest(String),
    }

    impl IntoResponse for HandlerError {
        fn into_response(self) -> axum::response::Response {
            let (code, msg) = match self {
                HandlerError::NotFound(obj) => (StatusCode::NOT_FOUND, obj),
                HandlerError::Unauthenticated => {
                    (StatusCode::UNAUTHORIZED, "Unauthorized".to_owned())
                }
                HandlerError::InvalidRequest(err) => (StatusCode::BAD_REQUEST, err),
                HandlerError::Internal(_) => (
                    StatusCode::INTERNAL_SERVER_ERROR,
                    "Internal error".to_owned(),
                ),
            };
            (code, msg).into_response()
        }
    }

    impl From<KikilistaApiError> for HandlerError {
        fn from(value: KikilistaApiError) -> Self {
            match value {
                KikilistaApiError::ListNotFound => {
                    HandlerError::NotFound("List not found".to_owned())
                }
                KikilistaApiError::ItemNotFound => {
                    HandlerError::NotFound("Item not found".to_owned())
                }
                KikilistaApiError::Internal(err) => HandlerError::Internal(err),
            }
        }
    }
}
