use std::sync::Arc;

use axum::{
    extract::{Request, State},
    http::HeaderMap,
    middleware::Next,
    response::Response,
};
use tower::{
    layer::util::{Identity, Stack},
    ServiceBuilder,
};
use tower_http::{
    classify::{ServerErrorsAsFailures, SharedClassifier},
    trace::TraceLayer,
};

use crate::{api::KikilistaApi, Token};

use super::handlers::error::HandlerError;

/// A middleware that looks for the authentication token before proceeding
///
pub async fn token_middleware<Api>(
    State(api): State<Arc<Api>>,
    headers: HeaderMap,
    request: Request,
    next: Next,
) -> Result<Response, HandlerError>
where
    Api: KikilistaApi,
{
    match headers.get("auth-token").map(|v| v.to_str()) {
        Some(Ok(token)) if api.authenticate(&Token::new(token))? => {}
        _ => return Err(HandlerError::Unauthenticated),
    }
    Ok(next.run(request).await)
}

pub fn trace_middleware(
) -> ServiceBuilder<Stack<TraceLayer<SharedClassifier<ServerErrorsAsFailures>>, Identity>> {
    ServiceBuilder::new().layer(TraceLayer::new_for_http())
}
