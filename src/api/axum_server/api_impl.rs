use std::sync::PoisonError;

use crate::{
    api::{
        AllListsRequest, AllListsResponse, ChangeItemStateRequest, ChangeItemStateResponse,
        NewItemRequest, NewItemResponse, NewListRequest, NewListResponse, RemoveItemRequest,
        RemoveItemResponse, RemoveListRequest, RemoveListResponse, ShowListRequest,
        ShowListResponse,
    },
    list, token,
};

use super::{AxumKikilistaServer, KikilistaApi, KikilistaApiError};

impl<L, T> KikilistaApi for AxumKikilistaServer<L, T>
where
    L: list::ListStorage + Send + Sync + 'static,
    T: token::TokenStorage + Send + Sync + 'static,
{
    fn authenticate(&self, token: &token::Token) -> Result<bool, KikilistaApiError> {
        Ok(self.token_storage.lock()?.owner(token)?.is_some())
    }

    fn new_list(&self, request: NewListRequest) -> Result<NewListResponse, KikilistaApiError> {
        let id = self.list_storage.lock()?.new_list(request.name)?;
        Ok(NewListResponse { id })
    }

    fn all_lists(&self, _: AllListsRequest) -> Result<AllListsResponse, KikilistaApiError> {
        let lists = self.list_storage.lock()?.all_lists()?;
        Ok(AllListsResponse { lists })
    }

    fn show_list(&self, request: ShowListRequest) -> Result<ShowListResponse, KikilistaApiError> {
        let list = self
            .list_storage
            .lock()?
            .read_list(request.id)?
            .ok_or(KikilistaApiError::ListNotFound)?;
        Ok(ShowListResponse { list })
    }

    fn remove_list(
        &self,
        request: RemoveListRequest,
    ) -> Result<RemoveListResponse, KikilistaApiError> {
        if !self.list_storage.lock()?.remove_list(request.id)? {
            return Err(KikilistaApiError::ListNotFound);
        }
        Ok(RemoveListResponse {})
    }

    fn new_item(&self, request: NewItemRequest) -> Result<NewItemResponse, KikilistaApiError> {
        let id = self
            .list_storage
            .lock()?
            .new_item(request.list_id, request.name)?;
        Ok(NewItemResponse { id })
    }

    fn remove_item(
        &self,
        request: RemoveItemRequest,
    ) -> Result<RemoveItemResponse, KikilistaApiError> {
        if !self
            .list_storage
            .lock()?
            .remove_item(request.list_id, request.item_id)?
        {
            return Err(KikilistaApiError::ItemNotFound);
        }
        Ok(RemoveItemResponse {})
    }

    fn update_item(
        &self,
        request: ChangeItemStateRequest,
    ) -> Result<ChangeItemStateResponse, KikilistaApiError> {
        self.list_storage
            .lock()?
            .update_item(request.list_id, request.item_id, request.state)?;
        Ok(ChangeItemStateResponse {})
    }
}

impl From<list::ListStorageError> for KikilistaApiError {
    fn from(value: list::ListStorageError) -> Self {
        match value {
            list::ListStorageError::ListNotFound => KikilistaApiError::ListNotFound,
            list::ListStorageError::Internal(err) => KikilistaApiError::Internal(err),
        }
    }
}

impl From<token::TokenStorageError> for KikilistaApiError {
    fn from(value: token::TokenStorageError) -> Self {
        KikilistaApiError::Internal(value.to_string())
    }
}

impl<T> From<PoisonError<T>> for KikilistaApiError {
    fn from(value: PoisonError<T>) -> Self {
        Self::Internal(value.to_string())
    }
}
